<?php
/**
 * Підключаємо локалізацію шаблону
 */
load_theme_textdomain('cl', get_template_directory() . '/languages');

/**
 * Додаємо favico, charset, viewport
 */
function add_head_meta_tags()
{
    ?>
    <link rel="shortcut icon" href="<?php bloginfo("template_url"); ?>/favicon.ico" type="image/x-icon">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php
}
add_action('wp_head', 'add_head_meta_tags');

/**
 * Підключаємо css файли
 */
function add_theme_style(){
    wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/fonts/font-awesome/css/font-awesome.min.css');
    wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css');
    wp_enqueue_style( 'styleSocialFont', get_template_directory_uri() . '/js/social-share-urls-master/font.css');
}
add_action( 'wp_enqueue_scripts', 'add_theme_style' );

/**
 * Підключаємо javascript файли
 */
function add_theme_scripts(){
    wp_enqueue_script("jquery");
    wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery') );
    wp_enqueue_script( 'cssClassChanger', get_template_directory_uri() . '/js/cssClassChanger.js' );
    wp_enqueue_script( 'main', get_template_directory_uri() . '/js/main.js' );
    wp_enqueue_script('social', get_template_directory_uri() . '/js/social-share-urls-master/generator.js');
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );

/**
 * Реєструємо місце для меню
 */

register_nav_menus( array(
    'primary' => __('Primary', 'cl')
    ));

/**
 * Реєструємо сайдбари теми
 */
if ( function_exists('register_sidebar') )
{
    register_sidebars(1, array(
        'id' => 'header_sidebar_left',
        'name' => __('Header left', 'cl'),
        'description' => 'put header left widgets here',
        'before_widget' => '<div class="widget">',
        'after_widget' => '</div>',
        'before_title' => '<p class="widget-title">',
        'after_title' => '</p>'
    ));

    register_sidebars(1, array(
        'id' => 'header_sidebar_right',
        'name' => __('Header right', 'cl'),
        'description' => 'put header right widgets here',
        'before_widget' => '<div class="widget">',
        'after_widget' => '</div>',
        'before_title' => '<p class="widget-title">',
        'after_title' => '</p>'
    ));

	register_sidebars(1, array(
        'id' => 'right',
        'name' => __('Right sidebar', 'cl'),
        'description' => 'Displayed only on single post pages',
        'before_widget' => '<div class="widget">',
        'after_widget' => '</div>',
        'before_title' => '<p class="widget-title">',
        'after_title' => '</p>'
    ));

    register_sidebars(1, array(
        'id' => 'content_sidebar',
        'name' => __('content', 'cl'),
        'description' => 'put content full-width widgets here',
        'before_widget' => '<div class="widget">',
        'after_widget' => '</div>',
        'before_title' => '<p class="widget-title">',
        'after_title' => '</p>'
    ));

    register_sidebars(1, array(
        'id' => 'content_fixed_width_sidebar',
        'name' => __('content fixed width sidebar', 'cl'),
        'description' => 'put content widgets here'
    ));

    register_sidebars(1, array(
        'id' => 'content_fixed_width_sidebar_2',
        'name' => __('content fixed width sidebar №2', 'cl'),
        'description' => 'every widgets have 33% width here',
        'before_widget' => '<div class="col-sm-4">',
        'after_widget' => '</div>'
    ));

    register_sidebars(1, array(
        'id' => 'footer_sidebar',
        'name' => __('Footer', 'cl'),
        'description' => 'put footer widgets here',
        'before_widget' => '<div class="widget col-sm-3">',
        'after_widget' => '</div>',
        'before_title' => '<p class="widget-title">',
        'after_title' => '</p>'
    ));
}

/**
 * Підключаємо підтримку мініатюр
 */
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size( 900, 300 );
}
if ( function_exists( 'add_image_size' ) ) { 
    add_image_size( 'photo', 900, 300, true );
}

/**
 * Реєструємо формати постів
 */
function add_post_formats(){
    add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat' ) );
    add_theme_support( 'post-thumbnails' );
    add_theme_support( 'custom-background' );
    add_theme_support( 'custom-header' );
    add_theme_support( 'custom-logo' );
}
add_action( 'after_setup_theme', 'add_post_formats', 11 );

/**
 * Замінюємо стандартне закінчення обрізаного тексту з [...] на ...
 */
function custom_excerpt_more( $more ) {
    return ' ...';
}
add_filter( 'excerpt_more', 'custom_excerpt_more' );

/**
 * Замінюємо стандартну довжину обрізаного тексту
 */
function custom_excerpt_length( $length ) {
    return 150;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

/**
 * Службова функція для опціонального відображення заголовків віджетів
 */
function displayWidgetsTitle($that, $instance){
    $tableId=$that->get_field_id("title");
    $tableName=$that->get_field_name("title");
    $displayTitleId=$that->get_field_id('displayTitle');
    $displayTitleName=$that->get_field_name('displayTitle');
    ?>
    <p>
        <label for="<?php echo $tableId ?>">Widget title</label><br>
        <input class="widefat" id="<?php echo $tableId; ?>" type="text" name="<?php echo $tableName; ?>" value="<?php echo $instance['title']; ?>">
    </p>
    <p>
        <label for="<?php echo $displayTitleId ?>">
        <input type="checkbox" id="<?php echo $displayTitleId; ?>" name="<?php echo $displayTitleName; ?>" value="display" <?php if ($instance['displayTitle']=='display') echo 'checked';?>> Display title</label>
    </p><?php
}

/**
 * Віджет контактів
 */
add_action('widgets_init', 'contactWidget');

function contactWidget(){
    register_widget('contactWidget');
}

class contactWidget extends WP_Widget
{
    public function __construct()
    {
        $args = array(
            'description' => 'позволяет добавлять и выводить ваши контакты'
        );
        parent::__construct('contactWidget_id', 'contactWidget', $args);
    }
    public function form($instance)
    {
        $phoneId=$this->get_field_id('phone');
        $phoneName=$this->get_field_name('phone');
        $emailId=$this->get_field_id('email');
        $emailName=$this->get_field_name('email');
        $that=$this;
        displayWidgetsTitle($that, $instance);
        ?>
        <p>
            <label for="<?php echo $phoneId;?>">Phone number:</label><br>
            <input type="text" id="<?php echo $phoneId;?>" name="<?php echo $phoneName;?>" value="<?php echo $instance['phone']?>" class="widefat">
        </p>
        <p>
            <label for="<?php echo $emailId;?>">Email:</label><br>
            <input type="text" id="<?php echo $emailId;?>" name="<?php echo $emailName;?>" value="<?php echo $instance['email']?>" class="widefat">
        </p>
        <?php
    }
    public function widget($args, $instance)
    {
        ?>
        <div class="top-navigation-left-text">
            <?php if ($instance['displayTitle']=='display') echo '<h4 style="margin: 0px 10px;">' . $instance['title'] . '</h4>';?>
            <div style="margin: 0px 10px; display: inline-block; *display: inline; *zoom:1;">
                <i class="gdlr-icon icon-phone fa fa-phone" style="color: #bababa; font-size: 14px; "></i> <?php echo $instance['phone']; ?>
            </div>
            <div style="margin: 0px 10px ; display: inline-block; *display: inline;  *zoom:1;">
                <i class="gdlr-icon icon-envelope fa fa-envelope" style="color: #bababa; font-size: 14px; "></i> <?php echo $instance['email']; ?>
            </div>
        </div>
        <?php
    }
    public function update($newInstance, $oldInstance) {
        $values = array();
        $values['title'] = htmlentities($newInstance['title']);
        $values['displayTitle'] = htmlentities($newInstance['displayTitle']);
        $values['phone'] = htmlentities($newInstance['phone']);
        $values['email'] = htmlentities($newInstance['email']);
        return $values;
    }
}

/**
 * Віджет соціальних кнопок
 */
add_action('widgets_init', 'socialWidget');

function socialWidget(){
    register_widget('socialWidget');
}

class socialWidget extends WP_Widget
{

    public function __construct()
    {
        $args = array(
            'description' => 'позволяет добавлять и выводить ваши социальные ссылки'
        );
        parent::__construct('socialWidget_id', 'socialWidget', $args);
    }
    public function form($instance)
    {
        $facebookId=$this->get_field_id('facebook');
        $facebookName=$this->get_field_name('facebook');
        $googleId=$this->get_field_id('google');
        $googleName=$this->get_field_name('google');
        $linkedInId=$this->get_field_id('linkedIn');
        $linkedInName=$this->get_field_name('linkedIn');
        $twitterId=$this->get_field_id('twitter');
        $twitterName=$this->get_field_name('twitter');
        $that=$this;
        displayWidgetsTitle($that, $instance);
        ?>
        <p>
            <label for="<?php echo $facebookId;?>">Facebook link:</label><br>
            <input type="text" id="<?php echo $facebookId;?>" name="<?php echo $facebookName;?>" value="<?php echo $instance['facebook']?>" class="widefat">
        </p>
        <p>
            <label for="<?php echo $googleId;?>">Google+ link:</label><br>
            <input type="text" id="<?php echo $googleId;?>" name="<?php echo $googleName;?>" value="<?php echo $instance['google']?>" class="widefat">
        </p>
        <p>
            <label for="<?php echo $linkedInId;?>">LinkedIn link:</label><br>
            <input type="text" id="<?php echo $linkedInId;?>" name="<?php echo $linkedInName;?>" value="<?php echo $instance['linkedIn']?>" class="widefat">
        </p>
        <p>
            <label for="<?php echo $twitterId;?>">Twitter link:</label><br>
            <input type="text" id="<?php echo $twitterId;?>" name="<?php echo $twitterName;?>" value="<?php echo $instance['twitter']?>" class="widefat">
        </p>
        <?php
    }
    public function widget($args, $instance)
    {
        ?>
        <div class="top-navigation-left-text">
            <?php if ($instance['displayTitle']=='display') echo '<h4 style="margin: 0px 10px;">' . $instance['title'] . '</h4>';?>
            <?php if ($instance['facebook']!=''){?><div style="margin: 0px 10px; display: inline-block; *display: inline; *zoom:1;">
                <a href="<?php echo $instance['facebook'] ?>"><i class="gdlr-icon fa fa-facebook" aria-hidden="true" style="color: #bababa; font-size: 14px; "></i></a>
            </div><?php } ?>
            <?php if ($instance['google']!=''){?><div style="margin: 0px 10px ; display: inline-block; *display: inline;  *zoom:1;">
                <a href="<?php echo $instance['google'] ?>"><i class="gdlr-icon fa fa-google-plus" aria-hidden="true" style="color: #bababa; font-size: 14px; "></i></a>
            </div><?php }?>
            <?php if ($instance['linkedIn']!=''){?><div style="margin: 0px 10px ; display: inline-block; *display: inline;  *zoom:1;">
                <a href="<?php echo $instance['linkedIn'] ?>"><i class="gdlr-icon fa fa-linkedin" aria-hidden="true" style="color: #bababa; font-size: 14px; "></i></a>
                </div><?php }?>
            <?php if ($instance['twitter']!=''){?><div style="margin: 0px 10px ; display: inline-block; *display: inline;  *zoom:1;">
                <a href="<?php echo $instance['twitter'] ?>"><i class="gdlr-icon fa fa-twitter" aria-hidden="true" style="color: #bababa; font-size: 14px; "></i></a>
                </div><?php }?>
        </div>
        <?php
    }
    public function update($newInstance, $oldInstance) {
        $values = array();
        $values['title'] = htmlentities($newInstance['title']);
        $values['displayTitle'] = htmlentities($newInstance['displayTitle']);
        $values['facebook'] = htmlentities($newInstance['facebook']);
        $values['google'] = htmlentities($newInstance['google']);
        $values['linkedIn'] = htmlentities($newInstance['linkedIn']);
        $values['twitter'] = htmlentities($newInstance['twitter']);
        return $values;
    }
}

/**
 * підправляємо вивід віджетів в футері
 */
add_action( 'dynamic_sidebar_before', 'before_footer_widgets', 10, 2 );
function before_footer_widgets( $index, $has_widgets ){
    if ($index=='footer_sidebar' && $has_widgets) {
        echo '<div class="row">';
    }
}
add_action( 'dynamic_sidebar_after', 'after_footer_widgets', 10, 2 );
function after_footer_widgets( $index, $has_widgets ){
    if ($index=='footer_sidebar' && $has_widgets) {
        echo '</div>';
    }
}
/**
 *  Віджет для виводу останніх постів з вибраних таксономій
 */
add_action('widgets_init', 'clpw');

function clpw(){
    register_widget('clpw');
}

class clpw extends WP_Widget
{

    public function __construct()
    {
        $args = array(
            'description' => 'выводит список заголовков последних постов заданой категории'
        );
        parent::__construct('clpw_id', 'category last posts widget', $args);
    }
    public function form($instance)
    {
        $count=(isset($instance['count']) && $instance['count']!='' && $instance['count']>1) ? $instance['count'] : 2;
        $that=$this;
        displayWidgetsTitle($that, $instance);
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('count');?>">Количество элементов в списке</label>
            <input type="number" id="<?php echo $this->get_field_id('count');?>" name="<?php echo $this->get_field_name('count');?>" value="<?php echo $count?>" class="widefat">
        </p>

        <?php
        $terms = get_terms( 'category' );
        $categories=[];
        foreach ($terms as $tm){
            $categories[]=['name'=>$tm->name, 'slug'=>$tm->slug];
        }
        ?>
        <select style="width:100%;" name="<?php echo $this->get_field_name('cat');?>">
            <?php foreach ($categories as $c){
             echo '<option value="' . $c['name'] . '">' . $c['name'] . '</option>';
            } ?>
        </select><br><br>
        <?php
    }
    public function widget($args, $instance)
    {
        $the_query = new WP_Query( array( 'category_name' => $instance['cat'],
            'posts_per_page' => $instance['count']) );
        echo $args['before_widget'];
        ?>
           <div class="">
                <?php if ($instance['displayTitle']=='display') echo '<p>' . $instance['title'] . '</p>';?>
                <?php
                if ( $the_query->have_posts() ) {
                    echo '<div>';
                    while ( $the_query->have_posts() ) {
                        $the_query->the_post();
                        ?>
                        <div class="pull-left">
                        <a href="<?php the_permalink()?>"> <img src="<?php the_post_thumbnail_url() ?>" style="height: 70px; width: 70px;"></a>
                        </div>
                        <div style="padding-left: 80px;">
                             <a class="" href="<?php the_permalink()?>"><p><?php the_title() ?></p></a> Posted on <?php the_time('j F Y'); ?>
                        </div><br><br>
                        <div class="clearfix" style="border-bottom: 1px solid #bababa;"></div><br>
                        <?php

                    }
                    echo '</div>';
                    /* Restore original Post Data */
                    wp_reset_postdata();
                } else {
                    // no posts found
                }
                ?>
            </div>
        <?php echo $args['after_widget'];

    }
    public function update($newInstance, $oldInstance) {
        $values = array();
        $values["title"] = htmlentities($newInstance["title"]);
        $values['displayTitle'] = htmlentities($newInstance['displayTitle']);
        $values["cat"] = htmlentities($newInstance["cat"]);
        $values["count"] = htmlentities($newInstance["count"]);
        return $values;
    }
}

/**
 * Широкий текстовий віджет для контента
 */
add_action('widgets_init', 'contentFullWidthWidget');

function contentFullWidthWidget(){
    register_widget('contentFullWidthWidget');
}

class contentFullWidthWidget extends WP_Widget
{
    public function __construct()
    {
        $args = array(
            'description' => 'выводит заданые заголовок, описание и ссылку на статью/страницу'
        );
        parent::__construct('contentFullWidthWidget_id', 'Content Full Width Widget', $args);
    }
    public function form($instance)
    {
        $descriptionId=$this->get_field_id('description');
        $descriptionName=$this->get_field_name('description');
        $linkId=$this->get_field_id('link');
        $linkName=$this->get_field_name('link');
        $that=$this;
        displayWidgetsTitle($that, $instance);
        ?>
        <p>
            <label for="<?php echo $descriptionId;?>">Description:</label><br>
            <input type="text" id="<?php echo $descriptionId;?>" name="<?php echo $descriptionName;?>" value="<?php echo $instance['description']?>" class="widefat">
        </p>
        <p>
            <label for="<?php echo $linkId;?>">link url:</label><br>
            <input type="text" id="<?php echo $linkId;?>" name="<?php echo $linkName;?>" value="<?php echo $instance['link']?>" class="widefat">
        </p>
        <?php
    }
    public function widget($args, $instance)
    {
        ?>
        <div style="margin-top:15px; margin-bottom:15px;" class="container-fluid content_widget">
            <div class="container-thin">
                <div class="pull-left-sm">
                    <?php if ($instance['displayTitle']=='display') echo '<h3>' . $instance['title'] . '</h3>';?>
                    <p><?php echo $instance['description']; ?></p>
                </div>
                <div class="pull-right-sm">
                    <a href="<?php echo $instance['link']; ?>"><input style="color: #fff; background-color: #72d5cd; padding:25px"  type="button" value="LEARN MORE"></a>
                </div>
            </div>
        </div>
        <?php
    }
    public function update($newInstance, $oldInstance) {
        $values = array();
        $values['title'] = htmlentities($newInstance['title']);
        $values['displayTitle'] = htmlentities($newInstance['displayTitle']);
        $values['description'] = htmlentities($newInstance['description']);
        $values['link'] = htmlentities($newInstance['link']);
        return $values;
    }
}
/**
 * Bіджет для контента
 */
add_action('widgets_init', 'contentFixedWidthWidget');

function contentFixedWidthWidget(){
    register_widget('contentFixedWidthWidget');
}

class contentFixedWidthWidget extends WP_Widget
{
    public function __construct()
    {
        $args = array(
            'description' => 'выводит заданые заголовок, описание и картинку'
        );
        parent::__construct('contentFixedWidthWidget_id', 'content Fixed Width Widget', $args);
    }
    public function form($instance)
    {
        $descriptionId=$this->get_field_id('description');
        $descriptionName=$this->get_field_name('description');
        $linkId=$this->get_field_id('link');
        $linkName=$this->get_field_name('link');
        $imgWidthId=$this->get_field_id('imgwidth');
        $imgWidthName=$this->get_field_name('imgwidth');
        $imgHeightId=$this->get_field_id('imgheight');
        $imgHeightName=$this->get_field_name('imgheight');
        $widthName=$this->get_field_name('width');
        ?>
        <p>
            <label for="<?php echo $linkId;?>">Image url:</label><br>
            <input type="text" id="<?php echo $linkId;?>" name="<?php echo $linkName;?>" value="<?php echo $instance['link']?>" class="widefat">
            <label for="<?php echo $imgWidthId;?>">Image width(px), not required:</label><br>
            <input type="text" id="<?php echo $imgWidthId;?>" name="<?php echo $imgWidthName;?>" value="<?php echo $instance['imgwidth']?>" class="widefat">
            <label for="<?php echo $imgHeightId;?>">Image height(px), not required:</label><br>
            <input type="text" id="<?php echo $imgHeightId;?>" name="<?php echo $imgHeightName;?>" value="<?php echo $instance['imgheight']?>" class="widefat">

        </p>
        <?php
        $that=$this;
        displayWidgetsTitle($that, $instance);
        ?>
        <p>
            <label for="<?php echo $descriptionId;?>">Description:</label><br>
            <input type="text" id="<?php echo $descriptionId;?>" name="<?php echo $descriptionName;?>" value="<?php echo $instance['description']?>" class="widefat">
        </p>
        <p>
            Elements Width:<br>
           <label><input type="radio" name="<?php echo $widthName; ?>" value="3" <?php if($instance['width']==3) echo 'checked'; ?>>25%</label>
           <label><input type="radio" name="<?php echo $widthName; ?>" value="4" <?php if($instance['width']==4) echo 'checked'; ?>>33.33%</label>
           <label><input type="radio" name="<?php echo $widthName; ?>" value="6" <?php if($instance['width']==6) echo 'checked'; ?>>50%</label>
           <label><input type="radio" name="<?php echo $widthName; ?>" value="8" <?php if($instance['width']==8) echo 'checked'; ?>>66.66%</label>
           <label><input type="radio" name="<?php echo $widthName; ?>" value="9" <?php if($instance['width']==9) echo 'checked'; ?>>75%</label>
           <label><input type="radio" name="<?php echo $widthName; ?>" value="12" <?php if($instance['width']==12) echo 'checked'; ?>>100%</label>
        </p>
        <?php
    }
    public function widget($args, $instance)
    {
        ?>
        <div style="margin-top:15px; margin-bottom:15px; text-align: center" class="col-sm-<?php echo $instance['width']; ?>">
            <p>
                <img style="width:<?php echo $instance['imgwidth'] . 'px;'; ?> height:<?php echo $instance['imgheight'] . 'px;'; ?>" src="<?php echo $instance['link']; ?>">
            </p>
            <?php if ($instance['displayTitle']=='display') echo '<h3>' . $instance['title'] . '</h3>';?>
            <p><?php echo $instance['description']; ?></p>
        </div>
        <?php
    }
    public function update($newInstance, $oldInstance) {
        $values = array();
        $values['title'] = htmlentities($newInstance['title']);
        $values['displayTitle'] = htmlentities($newInstance['displayTitle']);
        $values['description'] = htmlentities($newInstance['description']);
        $values['link'] = htmlentities($newInstance['link']);
        $values['width'] = htmlentities($newInstance['width']);
        $values['imgwidth'] = htmlentities($newInstance['imgwidth']);
        $values['imgheight'] = htmlentities($newInstance['imgheight']);
        return $values;
    }
}
/**
 * віджет з айфреймом відео
 */
add_action('widgets_init', 'videoWidget');

function videoWidget(){
    register_widget('videoWidget');
}

class videoWidget extends WP_Widget
{
    public function __construct()
    {
        $args = array(
            'description' => 'выводит заданые заголовок, описание и видеоплеер'
        );
        parent::__construct('videoWidget_id', 'Video Widget', $args);
    }
    public function form($instance)
    {
        $descriptionId=$this->get_field_id('description');
        $descriptionName=$this->get_field_name('description');
        $linkId=$this->get_field_id('link');
        $linkName=$this->get_field_name('link');
        $that=$this;
        displayWidgetsTitle($that, $instance);
        ?>
        <p>
            <label for="<?php echo $descriptionId;?>">Description:</label><br>
            <input type="text" id="<?php echo $descriptionId;?>" name="<?php echo $descriptionName;?>" value="<?php echo $instance['description']?>" class="widefat">
        </p>
        <p>
            <label for="<?php echo $linkId;?>">video url:</label><br>
            <input type="text" id="<?php echo $linkId;?>" name="<?php echo $linkName;?>" value="<?php echo $instance['link']?>" class="widefat">
        </p>
        <?php
    }
    public function widget($args, $instance)
    {
        ?>
        <div class="col-sm-4">
        <?php if ($instance['displayTitle']=='display') echo '<h3>' . $instance['title'] . '</h3>';?>
        <iframe style="width:100%;" src="<?php echo $instance['link']; ?>"></iframe>
        <p><?php echo $instance['description']; ?></p>
        </div>
        <?php
    }
    public function update($newInstance, $oldInstance) {
        $values = array();
        $values['title'] = htmlentities($newInstance['title']);
        $values['displayTitle'] = htmlentities($newInstance['displayTitle']);
        $values['description'] = htmlentities($newInstance['description']);
        $values['link'] = htmlentities($newInstance['link']);
        return $values;
    }
}
/**
 * віджет з коротким профілем інструктора
 */
add_action('widgets_init', 'instructorsProfileWidget');

function instructorsProfileWidget(){
    register_widget('instructorsProfileWidget');
}

class instructorsProfileWidget extends WP_Widget
{
    public function __construct()
    {
        $args = array(
            'description' => 'выводит заданые заголовок виджета, и профиль выбраного инструктора'
        );
        parent::__construct('instructorsProfileWidget_id', 'instructors Profile Widget', $args);
    }
    public function form($instance)
    {
        $instructorName=$this->get_field_name('instructor');
        $that=$this;
        displayWidgetsTitle($that, $instance);
        $instructors=new WP_Query(array(
            'post_type' => 'instructor'
        ));
        if(!empty($instructors->posts)){
        ?>
        <select style="width:100%;" name="<?php echo $instructorName; ?>">
            <?php foreach ($instructors->posts as $i){
                echo '<option value="' . $i->ID . '">' . $i->post_title . '</option>';
            } ?>
        </select><br><br>
        <?php }
        else echo 'No instructor found';
    }
    public function widget($args, $instance)
    {
        if (empty($instance['instructor'])) {
            echo 'No instructor found';
            return;
        }
        $instructor=new WP_Query(array(
            'post_type' => 'instructor',
            'p' => $instance['instructor']
        ));
        $instructor->the_post();
        ?>
        <div class="col-sm-4">
            <?php if ($instance['displayTitle']=='display') echo '<h3>' . $instance['title'] . '</h3>'; ?>
            <div style="text-align: center">
                <img src="<?php the_post_thumbnail_url(); ?>" style="display: block; width: 100%">
                <h4><?php the_title(); ?></h4>
                <p><?php echo $instructor->posts[0]->description; ?></p>
                <a href="<?php the_permalink(); ?>"><input style="color: #fff; background-color: #72d5cd; padding:15px; text-align:center;"  type="button" value="VIEW PROFILE"></a><br><br>
            </div>
        </div>
        <?php
        wp_reset_postdata();
    }
    public function update($newInstance, $oldInstance) {
        $values = array();
        $values['title'] = htmlentities($newInstance['title']);
        $values['displayTitle'] = htmlentities($newInstance['displayTitle']);
        $values['instructor'] = htmlentities($newInstance['instructor']);
        return $values;
    }
}

$x=10;