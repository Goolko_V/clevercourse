<?php /* Template Name: Home */ ?>
<?php get_header(); ?>
<?php layerslider(1) ?>
<div class="container-fluid">
    <div class="container-thin row">
        <?php dynamic_sidebar('content_fixed_width_sidebar'); ?>
    </div>
</div>
<?php dynamic_sidebar('content_sidebar'); ?>
<div class="container-fluid">
    <div class="container-thin row">
        <?php dynamic_sidebar('content_fixed_width_sidebar_2'); ?>

    </div>
</div>
<?php echo do_shortcode('[mtostag]'); ?>

<?php get_footer(); ?>