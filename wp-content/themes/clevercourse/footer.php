<footer class="container-fluid">
    <div class="container-thin">
        <?php dynamic_sidebar('footer_sidebar'); ?>
    </div>
</footer>
<div class="container-thin copy">
    <p class="pull-left">
        Theme By Vitaly Goolko
    </p>
    <p class="pull-right">
        Copyright © <?php the_time('Y') ?> - All Right Reserved
    </p>
    <div class="clearfix"></div>
</div>
<?php wp_footer(); ?>
    </body>
</html>