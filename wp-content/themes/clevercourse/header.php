<!DOCTYPE html>
<html>
  <head>
      <title><?php wp_title(''); ?></title>
      <?php wp_head(); ?>
  </head>
    <body <?php body_class(); ?>>
    <div class="container-fluid bababa container-thin container-thin-top">
        <div class="row top-row">
            <div class="col-sm-6">
                <?php dynamic_sidebar('header_sidebar_left'); ?>
            </div>
            <div class="col-sm-6">
                <div class="pull-right-sm">
                    <div class="pull-right-sm signin">
                        <i class="fa fa-lock icon-lock"></i> <a href="#">Sign In</a> | <a href="#">Sign Up</a>
                    </div>
                    <div class="pull-right-sm"">
                        <?php dynamic_sidebar('header_sidebar_right'); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-thin fly_menu fly_menu_in_stay">
        <div class="pull-left-sm"><?php the_custom_logo();
            /*$menuItems=wp_get_nav_menu_items('Menu 1');
            if (!empty($menuItems)) {
                ?><ul class="primary_menu"><?php
            for ($i=0; $i<count($menuItems); $i++){
                ?><li><a href="<?php echo $menuItems[$i]->url; ?>"><?php echo $menuItems[$i]->title; ?></a><?php
                if ($menuItems[$i+1] && $menuItems[$i+1]->menu_item_parent==$menuItems[$i]->ID){
                    ?> > <ul>
                        <li><a href="<?php echo $menuItems[$i+1]->url; ?>"><?php echo $menuItems[$i+]->title; ?></a></li>
                    </ul> <?php
                }
                ?></li><?php }
            ?></ul><?php
            }*/
            ?></div>
        <!-- Navigation -->
        <div class="pull-right-sm"><?php wp_nav_menu( array(
        'menu' => 'Menu 1',
        'menu_class' => 'primary_menu',
        'theme_location'=>'Primary'
        ) );?>
        </div>
        <div class="clearfix"></div>
    </div>