window.onload=function(){
	
var flyMenu=document.querySelector('div.fly_menu');
var top=document.querySelector('div.container-thin-top').offsetHeight;

window.onscroll = function() {
  if (window.pageYOffset>top && cssClassChanger.in(flyMenu, 'fly_menu_in_stay') || window.pageYOffset<top && cssClassChanger.in(flyMenu, 'fly_menu_in_fly')) cssClassChanger.relay(flyMenu, 'fly_menu_in_fly fly_menu_in_stay');
}	

var primaryMenu=document.querySelector('ul.primary_menu');

findingAllUl(primaryMenu);

function findingAllUl(ul) {
	for (var i = 0; i < ul.childNodes.length; i++) {
		if(ul.childNodes[i].nodeType==1 && ul.childNodes[i].tagName=='LI'){
			for (var j = 0; j < ul.childNodes[i].childNodes.length; j++) {
				if(ul.childNodes[i].childNodes[j].nodeType==1 && ul.childNodes[i].childNodes[j].tagName=='UL'){
					ul.childNodes[i].j=j;
					if (cssClassChanger.in(ul, 'sub-menu')) { 
						ul.childNodes[i].innerHTML+='<div class="pull-right">></div>';
						ul.childNodes[i].onmouseover=function(){
							this.childNodes[this.j].style.display='block';
							this.childNodes[this.j].style.left=this.offsetWidth +'px';
							this.childNodes[this.j].style.top=0 +'px';	
						}
					} else {
						ul.childNodes[i].onmouseover=function(){
							this.childNodes[this.j].style.display='block';	
							if(cssClassChanger.in(flyMenu, 'fly_menu_in_stay')) this.childNodes[this.j].style.paddingTop=35+'px';
							else this.childNodes[this.j].style.paddingTop=20+'px';
						}	
					}
					ul.childNodes[i].onmouseleave=function(){
						this.childNodes[this.j].style.display='none';
					}
					findingAllUl(ul.childNodes[i].childNodes[j]);
					break;
				}
			}	

		}
	}
}

}
