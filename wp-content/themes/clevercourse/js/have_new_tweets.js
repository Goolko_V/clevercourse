var myajaxurl='/wp-admin/admin-ajax.php';
var container = document.querySelector('ul[data-last-tweet]');
var currentTweet=container.querySelector('li:first-child');

currentTweet.style.display = 'block';

function go() {
    var data = {
        action: 'have_new_tweets',
        last_tweet: container.getAttribute('data-last-tweet')
    };

    jQuery.post(myajaxurl, data, function (response) {
        result = JSON.parse(response);
        for (var i = 0; i < result.length; i++) {
            container.removeChild(container.querySelector('li:last-child'));
            var newTweet = document.createElement("li");
            newTweet.style.display ='none';
            newTweet.innerHTML ='<p>' + result[i]['post_content'] + '</p>' + '<p>' + result[i]['date'] + '</p>';
            container.insertBefore(newTweet, container.querySelector('li:first-child'));
            if (i == result.length - 1) { 
            	container.setAttribute('data-last-tweet', result[i]['post_title']);
            	animationReady=false;
            	if (currentTweet) currentTweet.style.display ='none';
            	currentTweet=container.querySelector('li:first-child');
            	currentTweet.style.display = 'block';
            	currentTweet.style.animation="tweetGoTop " + tweetsFlyDuration + "ms 1";
				setTimeout(clearAnimation, tweetsFlyDuration);
            }
        }
        /*if (result.length>0) console.log(result[result.length - 1]['post_title']);
        else console.log('have no tweets');*/
        setTimeout(go, 30000);
    });
}
setTimeout(go, 30000);

var headStyle, animationReady=true, tweetsFlyDiapason=currentTweet.offsetWidth/3, 
tweetsFlyVerticalDiapason=currentTweet.offsetHeight/2, tweetsFlyDuration=1500;

function dinamicalCSSbuilder() {
 	if (headStyle==undefined) {
		headStyle=document.createElement('style');
		headStyle.type="text/css";
		document.head.appendChild(headStyle);
	}
	headStyle.innerHTML='';
	headStyle.appendChild(document.createTextNode("@keyframes tweetGoLeft { from { left:0px; opacity:1; } to { left:-" + tweetsFlyDiapason + "px; opacity:0.3; }}"));
	headStyle.appendChild(document.createTextNode("@keyframes tweetGoRight { from { right:0px; opacity:1; } to { right:-" + tweetsFlyDiapason + "px; opacity:0.3; }}"));
	headStyle.appendChild(document.createTextNode("@keyframes tweetGoTop{ from { top:" + tweetsFlyVerticalDiapason + "px; opacity:03; } to { top:0px; opacity:1; }}"));
}

dinamicalCSSbuilder();

function clearAnimation(){
	currentTweet.style.animation='';
	animationReady=true;
}

function oldTweet(){
	currentTweet.style.display='none';
	if (currentTweet.nextElementSibling) currentTweet=currentTweet.nextElementSibling;
	else currentTweet=container.querySelector('li:first-child');
	currentTweet.style.display='block';
	currentTweet.style.animation="tweetGoRight " + tweetsFlyDuration + "ms 1 reverse";
	setTimeout(clearAnimation, tweetsFlyDuration);
}

function newTweet(){
	currentTweet.style.display='none';
	if (currentTweet.previousElementSibling) currentTweet=currentTweet.previousElementSibling;
	else currentTweet=container.querySelector('li:last-child');
	currentTweet.style.display='block';
	currentTweet.style.animation="tweetGoLeft " + tweetsFlyDuration + "ms 1 reverse";
	setTimeout(clearAnimation, tweetsFlyDuration);
}

document.getElementById('tweets-control-left').onclick=function(){
	if (animationReady) {
		animationReady=false;
		currentTweet.style.animation="tweetGoLeft " + tweetsFlyDuration + "ms 1";
		setTimeout(oldTweet , tweetsFlyDuration);
	}
}

document.getElementById('tweets-control-right').onclick=function(){
	if (animationReady) {
		animationReady=false;
		currentTweet.style.animation="tweetGoRight " + tweetsFlyDuration + "ms 1";
		setTimeout(newTweet , tweetsFlyDuration);
	}	
}