var cssClassChanger={	
	add : function(elem, classString){
		elem.className=(elem.className + " " + classString).trim().replace(/  +/g," ");
	},

	remove : function(elem, classString, clear){	
		var tempPresent=elem.className.trim().replace(/  +/g," ").split(" "), tempRemoveUs=classString.trim().replace(/  +/g," ").split(" ");
		for (var i = 0; i < tempPresent.length; i++) {
			for (var j = 0; j < tempRemoveUs.length; j++) {
				if (tempPresent[i]==tempRemoveUs[j]) {
					tempPresent.splice(i,1);
					if (!clear) tempRemoveUs.splice(j,1);
					i--;
					break;
				};
			};
		};	
		elem.className=tempPresent.join(" ");
		if (!clear) return tempRemoveUs.join(" ");	
	},

	in : function(elem, classString){
		var tempPresent=elem.className.trim().replace(/  +/g," ").split(" "), tempRemoveUs=classString.trim().replace(/  +/g," ").split(" ");
		for (var i = 0; i < tempPresent.length; i++) {
			for (var j = 0; j < tempRemoveUs.length; j++) {
				if (tempPresent[i]==tempRemoveUs[j]) return true;
			};
		};
		return false;
	},

	relay : function(elem, classString){
		this.add(elem, this.remove(elem, classString));
	}
};