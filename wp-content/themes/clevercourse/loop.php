<?php if (have_posts()) : ?>
    <?php while (have_posts()) : the_post(); ?>
        <h2>
            <a href="<?php the_permalink()?>"><?php the_title(); ?></a>
        </h2>
        <p class="lead">
            by <?php the_author_posts_link(); ?>
        </p>
        <p><span class="glyphicon glyphicon-time"></span> Posted on <?php the_time('F j, Y g:i a'); ?></p>
        <hr>
        <img class="img-responsive" src="<?php the_post_thumbnail_url(array(900, 300))?>" alt="">
        <hr>
        <p><?php the_content();?></p>
        <a class="btn btn-primary" href="<?php the_permalink()?>">Read More <span class="glyphicon glyphicon-chevron-right"></span></a>
       <!-- <script src="//yastatic.net/es5-shims/0.0.2/es5-shims.min.js"></script>
        <script src="//yastatic.net/share2/share.js"></script>
        <div style="font-size: 60px!important;" class="ya-share2 pull-right" data-services="vkontakte,facebook,odnoklassniki,gplus,twitter,linkedin"></div><br>
        <br>-->
        <div class="social_links pull-right">
            <a class="twitter" href="https://twitter.com/intent/tweet?url=<?php echo the_permalink();?>&text=<?php echo the_title(); ?>&hashtags=<?php
            $t = wp_get_post_tags($post->ID);
            print_r($t[0]->name);?>"></a>
            <a class="google" href="https://plus.google.com/share?url=<?php echo the_permalink();?>"></a>
            <a class="facebook" href="https://www.facebook.com/sharer.php?u=<?php echo the_permalink();?>"></a>
            <a class="linkedin" href="https://www.linkedin.com/shareArticle?url=<?php echo the_permalink();?>&title=<?php echo the_title(); ?>"></a>
            <a class="pinterest" href="https://pinterest.com/pin/create/bookmarklet/?url=<?php echo the_permalink();?>&&description=<?php echo the_title(); ?>"></a>
        </div>
        <hr>
    <?php endwhile; ?>
<?php endif; ?>

