<?php
/**
 * Plugin Name: my_wp_tweet_pars
 * Description: пишет новые твиты в базу и выводит
 * Plugin URI:  Ссылка на инфо о плагине
 * Author URI:  Ссылка на автора
 * Author:      Goolko Vitaliy
 * Version:     1.0
 */

function cptui_register_my_cpts_tweet() {

    /**
     * Post Type: tweet.
     */

    $labels = array(
        "name" => __( 'tweet', '' ),
        "singular_name" => __( 'tweets', '' ),
    );

    $args = array(
        "label" => __( 'tweet', '' ),
        "labels" => $labels,
        "description" => "",
        "public" => true,
        "publicly_queryable" => true,
        "show_ui" => true,
        "show_in_rest" => false,
        "rest_base" => "",
        "has_archive" => false,
        "show_in_menu" => true,
        "exclude_from_search" => false,
        "capability_type" => "post",
        "map_meta_cap" => true,
        "hierarchical" => false,
        "rewrite" => array( "slug" => "tweet", "with_front" => true ),
        "query_var" => true,
        "supports" => array( "title", "editor", "custom-fields" ),
    );

    register_post_type( "tweet", $args );
}

add_action( 'init', 'cptui_register_my_cpts_tweet' );

add_option('tweet_name','');
add_option('tweet_count','');
////////////////////////////////////////////////////////////
add_filter( 'cron_schedules', 'cron_add_minutes' );

function cron_add_minutes( $schedules ) {
    $schedules['minutes'] = array(
        'interval' => 60,
        'display' => __( 'every_min' )
    );
    return $schedules;
}
//var_dump(wp_get_schedules());

add_action( 'check_twits', 'get_new_tweets' );

if (!wp_next_scheduled('check_twits')) {

    wp_schedule_event(time(), 'minutes', 'check_twits');

}

function get_new_tweets() {
    if (get_option('tweet_name')==false) return;

    require_once(__DIR__ . '/TwitterAPIExchange.php');

    $settings = array(
        'oauth_access_token' => "852516753687150593-2tkRRDGMwiGQj3KRuXz5p90zboPuzi5",
        'oauth_access_token_secret' => "Obhzg0Yy4EZB9zR9QqVmNM0YJiRMCzWIOGCOsoLZv8JNP",
        'consumer_key' => "njHTxvgXn5fwBzZgl4ubjCQE4",
        'consumer_secret' => "SmJIDMycUv2qruDJx6G8kiV2TNkUaCTnzk1oJsOUcIRHWewxe1"
    );

    $last_tweet=new WP_Query(array( 'post_type' => 'tweet',
        'posts_per_page' => 1,
        'orderby' => 'title',
        'order'   => 'DESC'
    ));
    //var_dump($last_tweet->posts[0]->post_title);

    $url = 'https://api.twitter.com/1.1/statuses/user_timeline.json';
    //screen_name=marusya199
    if ($last_tweet->posts) {
        $getfield = '?screen_name='. get_option('tweet_name') . '&count=10&since_id=' . $last_tweet->posts[0]->post_title;
    } else {
        $getfield = '?screen_name='. get_option('tweet_name') . '&count=10';
    }

    $requestMethod = 'GET';
    $twitter = new TwitterAPIExchange($settings);

    $newTwittsPack=array_reverse(json_decode($twitter->setGetfield($getfield)
        ->buildOauth($url, $requestMethod)
        ->performRequest()));

    foreach ($newTwittsPack as $singletweet){
        // Создаем массив данных новой записи
        $post_data = array(
            'post_title' => $singletweet->id_str,
            'post_content'  => $singletweet->text,
            'post_status'   => 'publish',
            'post_author'   => $singletweet->user->name,
            'post_type' => 'tweet'
        );

        // Вставляем запись в базу данных
        $post_id = wp_insert_post( $post_data );
        $singletweet->created_at=substr($singletweet->created_at, 0, strripos($singletweet->created_at, ':')) . substr($singletweet->created_at, -5);
        add_post_meta( $post_id, 'date', $singletweet->created_at, true );
    }
}
///////////////////////////////////////////////////////////////////////////
function my_wp_tweet_pars_settings(){
    add_menu_page('twitter settings', 'twitter settings', 'manage_options', 'twitter_settings', 'add_twitter_settings', '', 40 );
    function add_twitter_settings(){
        if ( !empty($_POST['tweet_name']) && check_admin_referer('update_twitter_settings_action','twitter_settings_nonce_field') )
        {
            update_option('tweet_name', $_POST['tweet_name']);
            if ($_POST['tweet_count']==false || $_POST['tweet_count']<0) update_option('tweet_count', 0);
            else update_option('tweet_count', $_POST['tweet_count']);
        }
        ?>
        <div style="width: 50%">
        <h3 xmlns="http://www.w3.org/1999/html">Twitter Settings</h3>
        <form id="twitter_settings" method="post">
           <p><label>User name  <input class="widefat" type="text" name="tweet_name" value="<?php echo get_option('tweet_name');?>"></label></p>
            <p><label>Display messages <input class="widefat" type="number" name="tweet_count" value="<?php echo get_option('tweet_count');?>"></label></p>
        <?php wp_nonce_field('update_twitter_settings_action','twitter_settings_nonce_field'); ?>
            <button type="submit">Save settings</button>
        </form>
        </div>
        <?php
    }
}

add_action('admin_menu', 'my_wp_tweet_pars_settings');
///////////////////////////////////////////////////////////////////////////

function myTweetsOutputShortcode(){

    $last_tweets=new WP_Query(array(
        'post_type' => 'tweet',
        'posts_per_page' => get_option('tweet_count'),
        'orderby' => 'title',
        'order'   => 'DESC'
    ));
    $last_tweet=$last_tweets->posts[0]->post_title;
    ?>
    <div class="container-fluid tweets_container">
        <div class="container-thin">
            <div class="clear tweets_title">
                <i id="tweets-control-left" class="fa fa-angle-left tweets-control"></i>
                <i class="fa fa-twitter"></i>
                <i id="tweets-control-right" class="fa fa-angle-right tweets-control"></i>
            </div>
            <ul class="list-unstyled" data-last-tweet="<?php echo $last_tweet ?>">
               <?php
               foreach ($last_tweets->posts as $t){
                ?>
                   <li style="display: none;">
                       <p><?php echo $t->post_content ?></p>
                       <p><?php echo get_post_meta($t->ID, 'date')[0]; ?></p>
                   </li>
               <?php }?>
            </ul>
        </div>
    </div>
    <?php
}
add_shortcode('mtostag', 'myTweetsOutputShortcode');

/////////////////////////////////////////////////////////////////////////////

if( wp_doing_ajax() ){
    add_action('wp_ajax_have_new_tweets', 'have_new_tweets_callback');
    add_action('wp_ajax_nopriv_have_new_tweets', 'have_new_tweets_callback');
}

add_action('wp_footer', 'my_action_javascript', 99);
function my_action_javascript() {
    echo '<script type="text/javascript" src="' . get_template_directory_uri() .  '/js/have_new_tweets.js"></script>';
}

function have_new_tweets_callback() {
    $last_published_tweet = $_POST['last_tweet'];
    $last_tweets=new WP_Query(array( 'post_type' => 'tweet',
        'posts_per_page' => 5,
        'orderby' => 'title',
        'order'   => 'DESC'
    ));

    if (empty($last_tweets->posts)) wp_die();
    $result=[];
    foreach ($last_tweets->posts as $tweet){
        if ($last_published_tweet==$tweet->post_title) break;
        $tweet->date=get_post_meta($tweet->ID, 'date')[0];
        $result[]=$tweet;
    }
    print_r(json_encode(array_reverse($result)));
    wp_die();
}